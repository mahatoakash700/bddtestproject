package com.test.DriverFactory;

import com.test.configController.ConfigurationController;
import com.test.testDataController.DeviceDetails;
import com.test.testDataController.TestDataController;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.net.URL;

public class DriverInstance extends DriverMaster{
    static Logger log = LogManager.getLogger(DriverInstance.class);

    public static AndroidDriver driver;
    public static DesiredCapabilities desiredCapabilities;

    public static AndroidDriver getDriverInstance() {
        System.out.println("Inside DriverInstance..");
        desiredCapabilities = new DesiredCapabilities();
        for (String deviceId : DeviceDetails.deviceList) {
            try {
                if (DeviceDetails.deviceDetailsMap.get(deviceId).get("OSName").equalsIgnoreCase("Android")) {
                    desiredCapabilities.setCapability("appPackage", TestDataController.configDataLists.get("appPackage"));
                    desiredCapabilities.setCapability("appActivity", TestDataController.configDataLists.get("appActivity"));
                    desiredCapabilities.setCapability("udid", deviceId);
                    desiredCapabilities.setCapability("systemPort", DeviceDetails.deviceDetailsMap.get(deviceId).get("systemPort"));
//                    desiredCapabilities.setCapability("app", path_to_apk);
                    driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), desiredCapabilities);
                    androidDriverThreadLocal.set(driver);
//                    driver = new AndroidDriver(new URL(TestDataController.configDataLists.get("hubProtocol") + "://"
//                            + TestDataController.configDataLists.get("hubHost") + ":"
//                            + TestDataController.configDataLists.get("hubPort")
//                            + TestDataController.configDataLists.get("endPoint")), desiredCapabilities);
                    System.out.println("Driver Instance Created");
                } else if (DeviceDetails.deviceDetailsMap.get(deviceId).get("platformName").equalsIgnoreCase("ios")) {
                    desiredCapabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
                    desiredCapabilities.setCapability("appActivity", "com.amazon.mShop.navigation.MainActivity");
                    desiredCapabilities.setCapability("automationName", TestDataController.configDataLists.get("automationName"));
                    desiredCapabilities.setCapability("udid", deviceId);
                    desiredCapabilities.setCapability("systemPort", DeviceDetails.deviceDetailsMap.get(deviceId).get("systemPort"));
//                desiredCapabilities.setCapability("app", path_to_apk);
                    driver = new AndroidDriver(new URL(TestDataController.configDataLists.get("URL")), desiredCapabilities);
                    log.info("Driver Instance Created");
                }
            } catch (Exception e) {
                log.error("Exception has occurred in getDriverInstance(): " + e.getMessage());
                e.printStackTrace();
            }
        }
        return driver;
    }


}
