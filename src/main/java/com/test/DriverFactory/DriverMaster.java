package com.test.DriverFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class DriverMaster {
    public static ThreadLocal<AndroidDriver> androidDriverThreadLocal = new ThreadLocal<>();
    public static ThreadLocal<AndroidDriver<MobileElement>> mobileDriver = new ThreadLocal<AndroidDriver<MobileElement>>();
    public static ThreadLocal<IOSDriver> iosDriverThreadLocal = new ThreadLocal<>();

    public synchronized static void setAndroidDriver(AndroidDriver driver) {
        androidDriverThreadLocal.set(driver);
    }
    public synchronized static void setMobileDriver(AndroidDriver<MobileElement> driver) {
        mobileDriver.set(driver);
    }

    public static synchronized AndroidDriver getAndroidDriver() {
        return androidDriverThreadLocal.get();
    }
    public static synchronized AndroidDriver getMobileDriver() {
        return mobileDriver.get();
    }

    public static void main(String[] args) {
    }
}
