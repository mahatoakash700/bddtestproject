package com.test.testDataController;

import com.test.genericUtils.PropertyFileUtils;
import com.test.genericUtils.ServiceUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;


public class TestDataController {
    static Logger log = LogManager.getLogger(TestDataController.class);
    public static HashMap<String, String> configDataLists = new HashMap<>();


    public static void init() {
        cleanUp();
        new TestDataController().loadConfigData();
    }

    public static void cleanUp() {
        File[] listFiles = null;
        try {
            if (GlobalVariables.isMAC) {
                listFiles = new File(GlobalVariables.baseDirPath + "GridFactory/MAC/node/").listFiles();
            } else if (GlobalVariables.isWINDOWS) {
                listFiles = new File(GlobalVariables.baseDirPath + "GridFactory/Windows/node/").listFiles();
            }
            for (File file : listFiles) {
                if (file.exists()) {
                    FileUtils.forceDelete(file);
                }
            }
            listFiles = new File(GlobalVariables.baseDirPath + "logs/").listFiles();
            if(listFiles.length > 0){
                for (File file : listFiles) {
                    if (file.exists()) {
//                    String command = "rmdir " + file;
//                        FileUtils.forceDelete(file);
                        file.delete();
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception has occurred: " + e.getMessage());
        }
    }

    public static void createDirectory(String directory) {
        try {
            File dir = new File(directory);
            if (!dir.exists()) dir.mkdirs();
        } catch (Exception e) {
            log.error("Exception has occurred in createDirectory(): " + e.getMessage());
        }
    }

    public HashMap<String, String> loadConfigData() {
        return configDataLists = PropertyFileUtils.getAllPropertiesData(new String[]{"ConfigFile", "ServiceCommands"});
    }


    public static void main(String[] args) {
        cleanUp();
    }


}
