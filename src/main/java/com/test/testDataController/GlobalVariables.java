package com.test.testDataController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;

import java.io.File;

public class GlobalVariables {
    static Logger log = LogManager.getLogger(TestDataController.class);
    public static String configFile = "ConfigFile";
    public static boolean isTestDataLoaded = false;
    public static String baseDirPath = null;
    public static String gridPath = "";
    public static boolean isMAC = false;
    public static boolean isWINDOWS = false;
    public static boolean isUNIX = false;
    public static boolean isSOLARIS = false;


    public static void init() {
        GlobalVariables globalVariables = new GlobalVariables();
        globalVariables.loadBasePath();
        globalVariables.setUpLog();
        globalVariables.getSystemOS();

    }

    public void loadBasePath() {
        if (!isTestDataLoaded) {
            File file = new File(".");
            try {
                baseDirPath = file.getCanonicalPath();
                file = new File(baseDirPath);
                baseDirPath = baseDirPath.replace("\\", "/");
                baseDirPath = new String(baseDirPath.replaceAll("%20", " "));
                if (!baseDirPath.substring(baseDirPath.length() - 1, baseDirPath.length()).equalsIgnoreCase("/")) {
                    baseDirPath = baseDirPath + "/";
                }
                System.out.println("BaseDirPath: " + baseDirPath);

            } catch (Exception e) {
                System.out.println("Exception has occurred in loadBasePath(): " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void getSystemOS() {
        String osName = null;
        try {
            osName = System.getProperty("os.name").toLowerCase();
            System.out.println("os :" + osName);
//            boolean IS_WINDOWS = (osName.indexOf("win") >= 0);
//            boolean IS_MAC = (osName.indexOf("mac") >= 0);
//            boolean IS_UNIX = (osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0);
//            boolean IS_SOLARIS = (osName.indexOf("sunos") >= 0);

            if (osName.indexOf("win") >= 0) {
                isWINDOWS = true;
            } else if (osName.indexOf("mac") >= 0) {
                isMAC = true;
            } else if (osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0) {
                isUNIX = true;
            } else if (osName.indexOf("sunos") >= 0) {
                isSOLARIS = true;
            } else {
                log.info("system OS is not supported!!");
            }
        } catch (Exception e) {
            log.error("Exception in getSystemOS(): " + e.getMessage());
        }
    }

    public static void setUpLog() {
        try {
            File file = new File(GlobalVariables.baseDirPath + "log4j_config.xml");
            file.getParentFile().mkdirs();
            LoggerContext context = (LoggerContext) LogManager.getContext(false);
            context.setConfigLocation(file.toURI());
            ThreadContext.put("logFileName", "framework_log");
            log.info("log setup completed");
        } catch (Exception e) {
            log.error("Exception has occurred in setUplog(): " + e.getMessage());
        }
    }
}
