package com.test.testDataController;

import com.test.genericUtils.ServiceUtils;
import jdk.nashorn.internal.objects.Global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceDetails {
    static Logger log = LogManager.getLogger(DeviceDetails.class);

    public static HashMap<String, HashMap<String, String>> deviceDetailsMap = new HashMap<>();
    public static List<String> deviceList = new ArrayList<>();
    public static ProcessBuilder builder;
    public static Process process;
    public Set<String> deviceDetailsList = new HashSet<>();
    public String androidRealDeviceCommand = null;
    public String androidEmulatorDeviceCommand = null;
    public String iphoneRealDeviceCommand = null;
    public String iphoneSimulatorDeviceCommand = null;
    public String androidDeviceCommand = null;
    public String[] commands = new String[4];


    public static void init() {
        try {
            DeviceDetails deviceDetails = new DeviceDetails();
            deviceDetails.getConnectedDeviceList();
            deviceDetails.getDeviceOSName();
            deviceDetails.getDeviceName();
            deviceDetails.getDeviceOSVersion();
        } catch (Exception e) {
            log.error("Exception has occurred: " + e.getMessage());
        }
    }

    public void getConnectedDeviceList() {
        try {
            startADBServer();
            if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_emulators"))) {
                bringEmulatorsOnline();
            } else {
                getAllDevicesForAndroid();
            }
            if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_simulators"))) {
                bringSimulatorsOnline();
            }
            if (GlobalVariables.isMAC) {
                if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_simulators"))) {
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{TestDataController.configDataLists.get("get_devices_real_command_mac"),
                            TestDataController.configDataLists.get("get_devices_sim_command_mac")});
                } else {
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{TestDataController.configDataLists.get("get_devices_real_command_mac")});
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in getConnectedDeviceList():" + e.getMessage());
        }
        log.info("connected devices: "+deviceList.size());
    }

    public void getDeviceOSName() {
        try {
            for (String deviceId : deviceList) {
                if (GlobalVariables.isMAC) {
                    androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                            deviceId + " " + TestDataController.configDataLists.get("android_device_osname_command_endPoint");

                    iphoneRealDeviceCommand = TestDataController.configDataLists.get("iphone_realDevice_deviceInfo_shell_command") + " " +
                            deviceId + " " + TestDataController.configDataLists.get("iphone_realDevice_deviceInfo_OSName_endPoint");

                    iphoneSimulatorDeviceCommand = TestDataController.configDataLists.get("iphone_simulator_deviceDetails_command") + " " + deviceId;

                    commands[0] = androidDeviceCommand;
                    commands[1] = iphoneRealDeviceCommand;

                    if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_simulators"))) {
                        commands[2] = iphoneSimulatorDeviceCommand;
                    }
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(commands);
                }
                if (GlobalVariables.isWINDOWS) {
                    androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                            deviceId + " " + TestDataController.configDataLists.get("android_device_osname_command_endPoint");

                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{androidDeviceCommand});
                }
                for (String line : deviceDetailsList) {
                    if (line.contains("Android")) {
                        if (deviceDetailsMap.containsKey(deviceId)) {
                            log.info("deviceId: " + deviceId + " already present in device details map");
                            deviceDetailsMap.get(deviceId).put("OSName", line);
                        } else {
                            log.info("deviceId: " + deviceId + " not present, putting the entire data");
                            deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                put("OSName", line);
                            }});
                        }
                    }
                    if (line.trim().contains("iPhone")) {
                        if (deviceDetailsMap.containsKey(deviceId)) {
                            log.info("deviceId: " + deviceId + " already present in device details map");
                            deviceDetailsMap.get(deviceId).put("OSName", "iPhoneOS");
                        } else {
                            log.info("deviceId: " + deviceId + " not present, putting the entire data");
                            deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                put("OSName", "iPhoneOS");
                            }});
                        }
                        deviceDetailsMap.get(deviceId).put("platformName", "ios");
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception has occurred in getDeviceOSName(): " + e.getMessage());
        }
    }

    public void getDeviceOSVersion() {
        try {
            for (String deviceId : deviceList) {
                if (GlobalVariables.isMAC) {
                    if (deviceDetailsMap.get(deviceId).get("deviceName").equalsIgnoreCase("xiaomi")) {
                        androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_device_xiaomi_osversion_command_endPoint");
                    } else {
                        androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_device_osversion_command_endPoint");
                    }

                    iphoneRealDeviceCommand = TestDataController.configDataLists.get("iphone_simulator_deviceInfo_command") + " " + deviceId;

                    iphoneSimulatorDeviceCommand = TestDataController.configDataLists.get("iphone_simulator_deviceInfo_command") + " " + deviceId;

                    commands[0] = androidDeviceCommand;
                    commands[1] = iphoneRealDeviceCommand;
                    if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_simulators"))) {
                        commands[2] = iphoneSimulatorDeviceCommand;
                    }
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(commands);
                }
                if (GlobalVariables.isWINDOWS) {
                    if (deviceDetailsMap.get(deviceId).get("deviceName").equalsIgnoreCase("xiaomi")) {
                        androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_device_xiaomi_osversion_command_endPoint");
                    } else {
                        androidDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_device_osversion_command_endPoint");
                    }
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{androidDeviceCommand});
                }
                for (String lines : deviceDetailsList) {
                    if (lines.length() < 10) {
                        Pattern pattern = Pattern.compile("[0-9]{1,2}([,.][0-9]{1,2})*?$");
                        Matcher match = pattern.matcher(lines);
                        if (match.matches()) {
                            System.out.println(match.group());
                            if (deviceDetailsMap.containsKey(deviceId)) {
                                log.info("deviceId: " + deviceId + " already present in device details map");
                                deviceDetailsMap.get(deviceId).put("OSVersion", match.group());
                                break;
                            } else {
                                log.info("deviceId: " + deviceId + " not present, putting the entire data");
                                deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                    put("OSVersion", match.group());
                                }});
                            }
                            break;
                        }
                    }
                    if (lines.length() > 10) {
                        Pattern pattern = Pattern.compile("\\b([0-9]+(?:\\.[0-9]+)+)");
                        Matcher match = pattern.matcher(lines);
                        if (match.find()) {
                            System.out.println(match.group());
                            if (deviceDetailsMap.containsKey(deviceId)) {
                                log.info("deviceId: " + deviceId + " already present in device details map");
                                deviceDetailsMap.get(deviceId).put("OSVersion", match.group());
                                break;
                            } else {
                                log.info("deviceId: " + deviceId + " not present, putting the entire data");
                                deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                    put("OSVersion", match.group());
                                }});
                            }
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception has occurred in getDeviceOSVersion(): " + e.getMessage());
        }
    }

    public void getDeviceName() {
        try {
            for (String deviceId : deviceList) {
                if (GlobalVariables.isMAC) {
                    if (!deviceId.contains("emulator")) {
                        androidRealDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_realDevice_devicename_command_endPoint");
                    }
                    iphoneRealDeviceCommand = TestDataController.configDataLists.get("iphone_simulator_deviceInfo_command") + " " + deviceId;
                    iphoneSimulatorDeviceCommand = TestDataController.configDataLists.get("iphone_simulator_deviceInfo_command") + " " + deviceId;

                    commands[0] = androidRealDeviceCommand;
                    commands[1] = iphoneRealDeviceCommand;
                    if (Boolean.parseBoolean(TestDataController.configDataLists.get("enable_simulators"))) {
                        commands[2] = iphoneSimulatorDeviceCommand;
                    }
                    deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(commands);
                }
                if (GlobalVariables.isWINDOWS) {
                    if (!deviceId.contains("emulator")) {
                        androidRealDeviceCommand = TestDataController.configDataLists.get("android_device_shell_command") + " " +
                                deviceId + " " + TestDataController.configDataLists.get("android_realDevice_devicename_command_endPoint");
                        deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{androidRealDeviceCommand});
                    }
                }
                for (String line : deviceDetailsList) {
                    String deviceName = "";
                    if (line.contains("iPhone")) {
                        Pattern pattern = Pattern.compile("(?<!\\()\\w+\\b(?![\\.\\-)])");
                        Matcher match = pattern.matcher(line);
                        while (match.find()) {
                            if (deviceName.equals("")) {
                                deviceName = match.group();
                            } else {
                                deviceName = deviceName.concat(match.group());
                            }
                            System.out.println(deviceName);
                        }
                        if (deviceDetailsMap.containsKey(deviceId)) {
                            log.info("deviceId: " + deviceId + " already present in device details map");
                            deviceDetailsMap.get(deviceId).put("deviceName", deviceName);
                        } else {
                            log.info("deviceId: " + deviceId + " not present, putting the entire data");
                            String finalDeviceName = deviceName;
                            deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                put("deviceName", finalDeviceName);
                            }});
                        }
                        break;

                    }
                    if (line.length() < 25) {
                        if (deviceDetailsMap.containsKey(deviceId)) {
                            log.info("deviceId: " + deviceId + " already present in device details map");
                            deviceDetailsMap.get(deviceId).put("deviceName", line);
                        } else {
                            log.info("deviceId: " + deviceId + " not present, putting the entire data");
                            deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                                put("deviceName", line);
                            }});
                        }
                        break;
                    }
                }
                if (deviceId.contains("emulator")) {
                    if (deviceDetailsMap.containsKey(deviceId)) {
                        log.info("deviceId: " + deviceId + " already present in device details map");
                        deviceDetailsMap.get(deviceId).put("deviceName", deviceId);
                    } else {
                        log.info("deviceId: " + deviceId + " not present, putting the entire data");
                        deviceDetailsMap.put(deviceId, new HashMap<String, String>() {{
                            put("deviceName", deviceId);
                        }});
                    }
                }
            }

        } catch (Exception e) {
            log.error("Exception occurred in getDeviceName(): " + e.getMessage());
        }
    }

    private void startADBServer() {
        try {
            String killCommand = TestDataController.configDataLists.get("adb_kill_server");
            String startCommand = TestDataController.configDataLists.get("adb_start_server");

            ServiceUtils.executeShellCommandWithWaitTime(new String[]{killCommand}, true, 3);
            ServiceUtils.executeShellCommandWithWaitTime(new String[]{startCommand}, true, 3);

        } catch (Exception e) {
            log.error("Exception occurred in startADBServer(): " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void bringEmulatorsOnline() {
        int count = 0;
        Set<String> emulatorlist;
        List<String> listOfEmulators = new ArrayList<>();
        List<Boolean> status = new ArrayList<>();
        try {
            if (GlobalVariables.isMAC) {

            }
            if (GlobalVariables.isWINDOWS) {
                String command = TestDataController.configDataLists.get("list_emulators");
                emulatorlist = ServiceUtils.fetchDataFromShellCommand(new String[]{command});
                if (Integer.parseInt(TestDataController.configDataLists.get("no_of_emulators")) > emulatorlist.size()) {
                    log.error("No of emulator present is less than required for testing");

                } else {
                    for (int i = 0; i < Integer.parseInt(TestDataController.configDataLists.get("no_of_emulators")); i++) {
                        String startEmulatorCommand = TestDataController.configDataLists.get("start_emulators") + new ArrayList<>(emulatorlist).get(i);
                        ServiceUtils.executeShellCommandUsingRuntime(new String[]{startEmulatorCommand});
                    }
                }

                do {
                    getAllDevicesForAndroid();
                    for (String device : deviceList) {
                        if (device.contains("emulator")) {
                            listOfEmulators.add(device);
                        } else {
                            Thread.sleep(5000);
                        }
                    }
                } while (listOfEmulators.size() < Integer.parseInt(TestDataController.configDataLists.get("no_of_emulators")) && (++count) < 3);
                if (listOfEmulators.size() < Integer.parseInt(TestDataController.configDataLists.get("no_of_emulators"))) {
                    log.info("Required no. of Emulators are not online");
                } else {
                    log.info("All Emulators are up and online");
                }
                count = 0;
                do {
                    String cmd = null;
                    for (String device : listOfEmulators) {
                        cmd = TestDataController.configDataLists.get("android_device_shell_command") + " " + device + " " +
                                TestDataController.configDataLists.get("android_device_online_status_command_end_point");
                        Set<String> result = ServiceUtils.fetchDataFromShellCommand(new String[]{cmd});
                        if (result.contains("1")) {
                            status.add(true);
                        } else {
                            Thread.sleep(5000);
                        }
                    }
                } while (status.stream().allMatch(val -> val == false) && (status.size() < listOfEmulators.size()) && (++count) < 3);
            }
        } catch (Exception e) {
            log.error("Exception has occurred in bringEmulatorsOnline(): " + e);
        }
    }

    public void getAllDevicesForAndroid() {
        try {
            deviceDetailsList = ServiceUtils.fetchDataFromShellCommand(new String[]{TestDataController.configDataLists.get("get_devices_command_android")});
            deviceDetailsList.removeAll(Arrays.asList("", null));
            for (String device : deviceDetailsList) {
                if (device.contains("List of devices attached"))
                    continue;
                if (device.endsWith("device") || device.endsWith("offline")) {
                    deviceList.add(device.split("\\t")[0]);
                    Thread.sleep(1000);
                    continue;
                }
                deviceList.add(device);
            }
        } catch (Exception e) {
            log.error("Exception in getAllDevices(): " + e.getMessage());
        }
    }

    private void bringSimulatorsOnline() {
    }
}
