package com.test.testClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestClass4 {

    private static String OS = System.getProperty("os.name").toLowerCase();
    public static boolean IS_WINDOWS = (OS.indexOf("win") >= 0);
    public static boolean IS_MAC = (OS.indexOf("mac") >= 0);
    public static boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    public static boolean IS_SOLARIS = (OS.indexOf("sunos") >= 0);


    public static void getOSVersion() throws IOException {
        ProcessBuilder builder = new ProcessBuilder("/bin/bash", "-c", "xcrun xctrace list devices");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        String[] device_os=null;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.contains("f9d56c2c8a6882156295223c8be8bb7744e19408")) {
 //               System.out.println(line);
                Pattern pattern = Pattern.compile("\\w+(?:\\.+\\w+)+");
                Matcher m = pattern.matcher(line);
                if(m.find()){
                    System.out.println(m.group());

                }

            }
        }
    }

    public static void deviceUDID() throws IOException {
        List<String> deviceList = new ArrayList<>();
        ProcessBuilder builder = new ProcessBuilder("/bin/bash", "-c", "adb devices");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        String device_id=null;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.endsWith("device")) {
                deviceList.add(line.split("\\t")[0]);
            }        }
        for (String device : deviceList) {
             System.out.println(device);
        }
    }

    public static void main(String[] args) throws IOException {
   //     deviceUDID();
        getOSVersion();
    }

//        System.out.println("os.name: " + OS);
//
//        if (IS_WINDOWS) {
//            System.out.println("This is Windows");
//        } else if (IS_MAC) {
//            System.out.println("This is Mac");
//        } else if (IS_UNIX) {
//            System.out.println("This is Unix or Linux");
//        } else if (IS_SOLARIS) {
//            System.out.println("This is Solaris");
//        } else {
//            System.out.println("Your OS is not support!!");
//        }
//    }

}
