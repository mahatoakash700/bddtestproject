package com.test.testClass;

import com.test.configController.ConfigurationController;
import com.test.testDataController.DeviceDetails;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;


public class TestClass3 {

    public static AndroidDriver driver;


    public static void main(String[] args) throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
//        desiredCapabilities.setCapability("platformName", "Android");
//        desiredCapabilities.setCapability("platformVersion", "10");
//        desiredCapabilities.setCapability("deviceName", "EMULATOR30X5X4X0");
        desiredCapabilities.setCapability("appPackage", "com.flipkart.android");
        desiredCapabilities.setCapability("appActivity", "com.flipkart.android.SplashActivity");
//        desiredCapabilities.setCapability("automationName", "UiAutomator2");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), desiredCapabilities);
    }
}
