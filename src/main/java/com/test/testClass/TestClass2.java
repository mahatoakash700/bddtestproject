package com.test.testClass;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TestClass2 {


    public static void main(String[] args) {
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("cmd /c netstat -ano | findstr 4728");

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            String line = null;
            if ((line = stdInput.readLine()) != null) {
                int index = line.lastIndexOf(" ");
                String pid = line.substring(index, line.length());
                System.out.println("Pid: " + pid);
                rt.exec("cmd /c Taskkill /PID" + pid + " /T /F");
                System.out.println("server stopped");
            }
            while ((line = stdError.readLine()) != null) {
                System.out.println(line);
            }

        } catch (Exception e) {
            System.out.println("something wrong with the server.Unable to stop");
        }
    }
}
