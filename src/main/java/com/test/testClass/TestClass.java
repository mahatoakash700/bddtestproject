package com.test.testClass;


import com.test.testDataController.DeviceDetails;
import com.test.testDataController.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestClass {

    static String message = null;

    private static void json() throws IOException {


        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject item = new JSONObject();
        item.put("browserName", "Android");
        item.put("platformVersion", "10");
        item.put("maxInstance", "3");
        array.put(item);
        json.put("capabilities", array);

        JSONObject item1 = new JSONObject();
        item1.put("cleanUpCycle", 2000);
        item1.put("timeOut", 30000);
        item1.put("proxy", "org.openqa.grid.selenium.proxy.DefaultRemoteProxy");

        json.put("configuration", item1);


        message = json.toString();


        System.out.println(message);

    }

    public static void convert() {
        String result = "true";
        System.out.println(Boolean.valueOf(result));
    }

    public static void main(String[] args) throws IOException {
        //    json();
        convert();
    }

}
