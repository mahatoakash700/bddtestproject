package com.test.pageLibrary.pageActions;

import com.test.DriverFactory.DriverMaster;
import com.test.Gestures;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class LaunchPage {
    public static Logger log = LogManager.getLogger(LaunchPage.class);
    public AndroidDriver driver;
    public WebDriverWait wait;
    public boolean result = false;

    public LaunchPage(AndroidDriver driver) {
        this.driver = driver;
        if(driver.getClass().getName().contains("AndroidDriver")){
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(30)), this);
        }else if (driver.getClass().getName().contains("IOSDriver")){

        }
        wait = new WebDriverWait(driver, 30);
    }

    @FindAll({
            @FindBy(className = "class1"),
            @FindBy(className = "class2")
    })
    private WebElement userName;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Welcome']"),
            @AndroidBy(id = "com.flipkart.android:id/title")
    })
    private MobileElement welcomeText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.Button[@text='CONTINUE']"),
            @AndroidBy(id = "com.flipkart.android:id/select_btn")
    })
    private MobileElement continueBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='English']"),
    })
    private MobileElement languageBtn;

    public boolean checkForWelcomePage() {
        return welcomeText.isDisplayed();
    }

    public boolean swipeUp() {
        try {
            Gestures.swipeScreen(Gestures.Direction.UP, driver);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkForContinueButton(Boolean enabled) {
        System.out.println("thread: "+Thread.currentThread().getId());
        try {
            if (!continueBtn.isEnabled()) {
                log.info("continue button is disabled");
                result = true;
            } else if (continueBtn.isEnabled()) {
                log.info("continue button is enabled");
            }
        } catch (Exception e) {
            log.error("Exception occurred in checkForContinueButton() " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkLanguageIsClicked(){
        try{
            if(languageBtn.isDisplayed()){
                languageBtn.click();
                result = true;
            }
        }catch (Exception e){
            log.error("Exception occurred in checkLanguageIsClicked() " + e.getMessage());
            e.printStackTrace();
        }
        log.info("Clicked on language");
        return result;
    }
    public boolean clickOnContinueButton(){
        try{
            if(continueBtn.isEnabled()){
                continueBtn.click();
                result = true;
            }
        }catch (Exception e){
            log.error("Exception occurred in checkLanguageIsClicked() " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}
