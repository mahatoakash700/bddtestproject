package com.test.pageLibrary.pageDefinitions;

import com.test.pageLibrary.pageActions.LaunchPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LaunchApp extends BasePage {

    @Before
    public void initialisePreRequisites() {
        super.beforeSuite();

    }

    @When("App is launched")
    public void appIsLaunched() {
    }

    @Then("LoginPage should be displayed")
    public void loginPageShouldBeDisplayed() {
        Assert.assertTrue(launchPage.checkForWelcomePage());
    }

    @Then("swipeUp to check all languages")
    public void swipeUpToCheckAllLanguages() {
        Assert.assertTrue(launchPage.swipeUp(), "swipeUpToCheckAllLanguages() is failed");
    }

    @And("check continue button is disabled")
    public void checkContinueButtonIsDisabled() {
        Assert.assertTrue(launchPage.checkForContinueButton(false), "com.flipkart.android:id/select_btn() is failed");
    }

    @Then("click on language")
    public void clickOnLanguage() {
        Assert.assertTrue(launchPage.checkLanguageIsClicked(),"unable to click on language btn");
    }

    @And("check Continue button is enabled")
    public void checkContinueButtonIsEnabled() {
        Assert.assertTrue(launchPage.checkForContinueButton(false), "com.flipkart.android:id/select_btn() is failed");
    }

    @Then("Click on Continue button")
    public void clickOnContinueButton() {
        Assert.assertTrue(launchPage.clickOnContinueButton(), "com.flipkart.android:id/select_btn() is failed");
    }
}
