package com.test.pageLibrary.pageDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginApp {
    @Given("User is at the Login Page")
    public void userIsAtTheLoginPage() {

    }

    @And("Navigate to LogIn Page")
    public void navigateToLogInPage() {
    }

    @When("User enter UserName and Password")
    public void userEnterUserNameAndPassword() {
    }

    @And("Click on the LogIn button")
    public void clickOnTheLogInButton() {
    }

    @Then("Successful LogIN message should display")
    public void successfulLogINMessageShouldDisplay() {
    }

    @When("User LogOut from the Application")
    public void userLogOutFromTheApplication() {
    }

    @Then("Successful LogOut message should display")
    public void successfulLogOutMessageShouldDisplay() {
    }
}
