package com.test.pageLibrary.pageDefinitions;

import com.test.DriverFactory.DriverInstance;
import com.test.configController.ConfigurationController;
import com.test.pageLibrary.pageActions.LaunchPage;
import com.test.testDataController.DeviceDetails;
import com.test.testDataController.GlobalVariables;
import com.test.testDataController.TestDataController;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import java.util.HashMap;

public class BasePage {
    public static Logger log = LogManager.getLogger(BasePage.class);
    public static HashMap<String, String> configData = new HashMap<>();
    public AndroidDriver driver;
    public LaunchPage launchPage;

    @Before
    public void beforeSuite() {
        GlobalVariables.init();
        TestDataController.init();
        DeviceDetails.init();
        ConfigurationController.init();
        driver = DriverInstance.getDriverInstance();
        launchPage = new LaunchPage(driver);
    }

    @After
    public void tearDOwn(){

    }
}
