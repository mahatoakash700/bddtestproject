package com.test.genericUtils;

import com.test.configController.ConfigurationController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;

public class PropertyFileUtils {
    static Logger log = LogManager.getLogger(PropertyFileUtils.class);

    public static String getDataFromProperties(String fileName, String key) {
        String data = null;
        try {
            File source = new File("./appConfig/" + fileName + ".properties");
            FileInputStream fis = new FileInputStream(source);
            Properties pro = new Properties();
            pro.load(fis);
            data = pro.getProperty(key);

        } catch (Exception e) {
            log.error("Exception has occurred in getDataFromProperties(): " + e.getMessage());
        }
        return data;
    }

    public static HashMap<String, String> getAllPropertiesData(String[] fileNameList) {
        HashMap<String, String> propertiesData = new HashMap<>();
        File source = null;
        FileInputStream fis = null;
        Properties pro =new Properties();
        for(String fileName : fileNameList){
            try {
                source = new File("./appConfig/" + fileName + ".properties");
                fis = new FileInputStream(source);
                pro.load(new StringReader(PropertyFileUtils.getFileContent(fis, "UTF8").replace("\\", "\\\\")));
                fis.close();

                for (String key : pro.stringPropertyNames()) {
                    propertiesData.put(key, pro.getProperty(key));
                }
            } catch (Exception e) {
                log.error("Exception has occurred in  getAllPropertiesData(): " + e.getMessage());
            }
        }
        return propertiesData;
    }

    public static void writePropertyValueIntoPropertiesFile(String fileName, String key, String value) throws Exception {
        try {
            File source = new File("./appConfig/" + fileName + ".properties");
            FileInputStream fis = new FileInputStream(source);
            Properties pro = new Properties();
            pro.load(fis);
            pro.setProperty(key, value);
            FileOutputStream fout = new FileOutputStream(source);
            pro.store(fout, "Edited property file stored");
        } catch (Exception e) {
            log.error("Exception has occurred in  writePropertyValueIntoPropertiesFile(): " + e.getMessage());
        }
    }

    public static String getFileContent(FileInputStream fis, String encoding) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(fis, encoding))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
        } catch (Exception e) {
            log.error("Exception has occurred in  getFileContent(): " + e.getMessage());
        }
        return sb.toString();
    }
}
