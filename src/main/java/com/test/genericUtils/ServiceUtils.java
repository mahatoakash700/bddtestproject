package com.test.genericUtils;

import com.test.testDataController.GlobalVariables;
import com.test.testDataController.TestDataController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServiceUtils {
    static Logger log = LogManager.getLogger(ServiceUtils.class);
    private static Process process;
    private static ProcessBuilder builder;
    private static BufferedReader stdInput;
    private static BufferedReader stdError;

    public static String getPidOfProcess(String[] commands, String processName) {
        String pid = null;
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac") + " ", TestDataController.configDataLists.get("common_command_mac") + " ", cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor();
                    }
                    if (GlobalVariables.isWINDOWS) {
                        commands = new String[]{TestDataController.configDataLists.get("common_command_windows") + " " + processName};
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_windows") + " ", TestDataController.configDataLists.get("common_command_mac") + " ", cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor();
                    }
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                    String line = "";
                    while ((line = stdInput.readLine()) != null) {
                        pid = line;
                    }
                    while ((line = stdError.readLine()) != null) {
//                log.info(line);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in getPidOfProcess(): " + e.getMessage());

        } finally {
            try {
                log.info("closing stream");
                stdInput.close();
                stdError.close();
                process.destroyForcibly();
                if (stdInput == null) {
                    log.info("readStream is closed");
                } else {
                    log.error("Unable to close readStream");
                }
                if (stdError == null) {
                    log.info("errorStream is closed");
                } else {
                    log.info("unable to close errorStream");
                }
            } catch (IOException e) {
                log.error("Exception occurred in closing streams in getPidOfProcess(): " + e);
            }
        }
        return pid;
    }

    public static Set<String> fetchDataFromShellCommand(String[] commands, String... value) {
        Set<String> data = new HashSet<>();
        BufferedReader stdInput = null;
        BufferedReader stdError = null;
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac"), TestDataController.configDataLists.get("common_command_mac"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
                    if (GlobalVariables.isWINDOWS) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_windows"), TestDataController.configDataLists.get("common_command_windows"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
                    try {
                        stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                        String line = "";
                        while ((line = stdInput.readLine()) != null) {
                            data.add(line);
                        }
                        while ((line = stdError.readLine()) != null) {
                            log.info(line);
                        }
                    } finally {
                        stdInput.close();
                        stdError.close();
                        if (stdInput == null) {
                            log.info("readStream1 is closed");
                        } else {
                            log.error("Unable to close readStream1");
                        }
                        if (stdError == null) {
                            log.info("errorStream is closed1");
                        } else {
                            log.info("unable to close errorStream1");
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in fetchDataFromShellCommand(): " + e.getMessage());

        } finally {
            process.destroyForcibly();
        }
        return data;
    }

    public static void executeShellCommand(String[] commands, String... value) {
        BufferedReader stdInput = null;
        BufferedReader stdError = null;
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac"), TestDataController.configDataLists.get("common_command_mac"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
                    if (GlobalVariables.isWINDOWS) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_windows"), TestDataController.configDataLists.get("common_command_windows"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
                    try {
                        stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                        String line = "";
//                while ((line = stdInput.readLine()) != null) {
//                    System.out.println(line);
//                }

                        while ((line = stdError.readLine()) != null) {
                            log.info(line);
                        }
                    }finally{
                        stdInput.close();
                        stdError.close();
                        if (stdInput == null) {
                            log.info("readStream is closed");
                        } else {
                            log.error("Unable to close readStream");
                        }
                        if (stdError == null) {
                            log.info("errorStream is closed");
                        } else {
                            log.info("unable to close errorStream");
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in executeShellCommand(): " + e.getMessage());

        } finally {
                process.destroyForcibly();
        }
    }

    public static void executeShellCommandWithWaitTime(String[] commands, boolean wait, long waitTime, String... value) {
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac"), TestDataController.configDataLists.get("common_command_mac"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        if (wait) {
                            process.waitFor(waitTime, TimeUnit.SECONDS);
                        }
                    }
                    if (GlobalVariables.isWINDOWS) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_windows"), TestDataController.configDataLists.get("common_command_windows"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        if (wait) {
                            process.waitFor(waitTime, TimeUnit.SECONDS);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in executeShellCommand(): " + e.getMessage());

        } finally {
          process.destroyForcibly();
        }
    }

    public static void executeMe(String[] commands) throws IOException, InterruptedException {
        for (String cmd : commands) {
            if (cmd != null) {
                builder = new ProcessBuilder("cmd.exe", "/c","start" ,cmd);
                builder.redirectErrorStream(true);
                process = builder.start();
                process.waitFor(5, TimeUnit.SECONDS);
                Thread.sleep(5000);
                System.out.println("waited 10 sec for device..");
            }
        }
    }

    public static void executeShellCommandUsingRuntime(String[] commands, String... value) {
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        process = Runtime.getRuntime().exec(TestDataController.configDataLists.get("base_command_mac") + " " + TestDataController.configDataLists.get("common_command_mac") + " " + cmd);
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
                    if (GlobalVariables.isWINDOWS) {
                        process = Runtime.getRuntime().exec(TestDataController.configDataLists.get("base_command_windows") + " " + TestDataController.configDataLists.get("common_command_windows") + " " + cmd);
                        process.waitFor(5, TimeUnit.SECONDS);
                    }
//                BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//                String line = "";
//                while ((line = stdError.readLine()) != null) {
//                    log.info(line);
//                }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in executeShellCommand(): " + e.getMessage());

        } finally {
            process.destroyForcibly();
        }
    }

    public static void executeShellCommandWithWaitTimeQuietly(String[] commands, boolean wait, long waitTime, String... value) {
        try {
            for (String cmd : commands) {
                if (cmd != null) {
                    if (GlobalVariables.isMAC) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac"), TestDataController.configDataLists.get("common_command_mac"), cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        if (wait) {
                            process.waitFor(waitTime, TimeUnit.SECONDS);
                        }
                    }
                    if (GlobalVariables.isWINDOWS) {
                        builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_windows"), TestDataController.configDataLists.get("common_command_windows"),"start",cmd);
                        builder.redirectErrorStream(true);
                        process = builder.start();
                        if (wait) {
                            process.waitFor(waitTime, TimeUnit.SECONDS);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in executeShellCommand(): " + e.getMessage());

        } finally {
            process.destroyForcibly();
        }
    }

    /***
     * read file for a string presence
     * @param filePath
     * @param stringToVerify
     * @param searchStart - start time to search
     * @param encoding - optional parameter to give encoding type
     * @return
     */
    public static Date parseLogFileForString(String filePath, String stringToVerify, Date searchStart, String... encoding) {
        System.out.println(stringToVerify);
        if (searchStart == null)
            searchStart = new Date();
        Date foundDateTime = null;
        Pattern date_pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
        Pattern time_pattern = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
        Matcher matcher;
        String time = "";
        String date = "";
        SimpleDateFormat dt_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date_time;
        //boolean isVerified = false;
        BufferedReader reader;
        String line = null;
        File f = null;
        //AgentReporter.logSuccess("Searching File - " + filePath);
        try {
            f = new File(filePath);
            if (!f.exists()) {
                System.out.println(filePath + " -> Not Exists");
            } else {
                System.out.println(filePath + " -> Checking");
                if ((encoding.length > 0) && (encoding[0] != null))
                    reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), encoding[0]));
                else
                    reader = new BufferedReader(new FileReader(filePath));
                while ((line = reader.readLine()) != null) {

                    matcher = date_pattern.matcher(line);
                    if (matcher.find()) {
                        date = matcher.group();
                        //System.out.println("Log Date - "+ date);
                    }
                    matcher = time_pattern.matcher(line);
                    if (matcher.find()) {
                        time = matcher.group();
                        //System.out.println("Log Time - "+ time);
                    }
                    date_time = dt_format.parse(date + " " + time);
                    if (date_time.getTime() >= searchStart.getTime()) {
                        if (line.contains(stringToVerify)) {
                            //isVerified = true;
                            foundDateTime = date_time;
                            log.info(stringToVerify + " is found in File - " + filePath + " at TimeStamp " + foundDateTime);
                            break;
                        }
                    }
                }
                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return foundDateTime;
    }

    public static boolean readStringInLog(String filePath, String stringToVerify) {
        boolean status = false;
        String line = null;
        File f = null;
        try {
            f = new File(filePath);
            if (!f.exists()) {
                System.out.println(filePath + " -> Not Exists");
            } else {
                System.out.println(filePath + " -> Checking");
                stdInput = new BufferedReader(new FileReader(filePath));
                while ((line = stdInput.readLine()) != null) {
                    if (line.contains(stringToVerify)) {
                        log.info("string: " + stringToVerify + " found");
                        status = true;
                        break;
                    } else {
                        log.info("string: " + stringToVerify + " not found in log");
                    }
                }
                if (line == null) {
                    Thread.sleep(5000);
                }
            }
        } catch (Exception e) {
            log.error("Exception occurred in readStringInLog(): " + e.getMessage());
        } finally {
            try {
                log.info("closing stream");
                stdInput.close();
//                stdError.close();
                if (stdInput == null) {
                    log.info("readStream is closed");
                } else {
                    log.error("Unable to close readStream");
                }
//                if(stdError == null){
//                    log.info("errorStream is closed");
//                }else{
//                    log.info("unable to close errorStream");
//                }
            } catch (IOException e) {
                log.error("Exception occurred in readStringInLog(): " + e);
            }
        }
        return status;
    }

    //    public void killTask() {
//        Process process = null;
//        try {
//            if(GlobalVariables.isMAC){
//                ProcessBuilder builder = new ProcessBuilder(TestDataController.configDataLists.get("base_command_mac")+" ", TestDataController.configDataLists.get("common_command_mac")+" ", cmd);
//                builder.redirectErrorStream(true);
//                process = builder.start();
//                process.waitFor();
//
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//                BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//                String line = "";
//                while ((line = bufferedReader.readLine()) != null) {
// //                   pid = line;
//                }
//                while ((line = stdError.readLine()) != null) {
////                log.info(line);
//                }
//            }
//
//        } catch (Exception e) {
//            log.error("Exception has occurred in killTask(): " + e.getMessage());
//        }
//    }
}