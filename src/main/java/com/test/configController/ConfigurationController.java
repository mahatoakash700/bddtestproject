package com.test.configController;


import com.test.genericUtils.ServiceUtils;
import com.test.testDataController.DeviceDetails;
import com.test.testDataController.GlobalVariables;
import com.test.testDataController.TestDataController;
import jdk.nashorn.internal.objects.Global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

enum Capabilities {
    browserName, platformVersion, maxInstance, platformName, automationName, deviceName, systemPort, deviceId,
    cleanUpCycle, timeOut, proxy, maxSession, register, registerCycle, hubPort, hubHost, hubProtocol, appPackage,
    appActivity, udid

}

public class ConfigurationController {
    static Logger log = LogManager.getLogger(ConfigurationController.class);
    public String nodeServiceFilePath = null;
    public String nodeRegisteredString = null;

    public static void init() {
        try {
            log.info("initializing ConfigurationController..");
            ConfigurationController configController = new ConfigurationController();
            configController.startServerGrid();
            configController.createDeviceCapabilitiesInJsonFile();
            configController.createExecutableFiles();
            configController.executeBatchFiles();
            configController.checkNodeRegisteredToGrid();
            configController.checkConnectedDeviceStatus();
            log.info("initialization completed");
        } catch (Exception e) {
            log.error("Exception has occurred during initialization: " + e.getMessage());
        }
    }

    public void createDeviceCapabilitiesInJsonFile() {
        try {
            int count = 0;
            for (String deviceId : DeviceDetails.deviceList) {
                JSONObject body = new JSONObject();
                JSONArray array = new JSONArray();
                JSONObject capabilities = new JSONObject();
                if (DeviceDetails.deviceDetailsMap.get(deviceId).get("OSName").equalsIgnoreCase("Android")) {
                    capabilities.put(String.valueOf(Capabilities.browserName), DeviceDetails.deviceDetailsMap.get(deviceId).get("OSName"));
                    capabilities.put(String.valueOf(Capabilities.platformName), DeviceDetails.deviceDetailsMap.get(deviceId).get("OSName"));
                    capabilities.put(String.valueOf(Capabilities.platformVersion), DeviceDetails.deviceDetailsMap.get(deviceId).get("OSVersion"));
                    capabilities.put(String.valueOf(Capabilities.automationName), TestDataController.configDataLists.get("automationName_android"));
                    capabilities.put(String.valueOf(Capabilities.deviceName), DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName"));
//                    capabilities.put(String.valueOf(Capabilities.appPackage), TestDataController.configDataLists.get("appPackage"));
//                    capabilities.put(String.valueOf(Capabilities.appActivity), TestDataController.configDataLists.get("appActivity"));
//                    capabilities.put(String.valueOf(Capabilities.udid), deviceId);
                } else if (DeviceDetails.deviceDetailsMap.get(deviceId).get("platformName").equalsIgnoreCase("ios")) {
                    capabilities.put(String.valueOf(Capabilities.browserName), DeviceDetails.deviceDetailsMap.get(deviceId).get("platformName"));
                    capabilities.put(String.valueOf(Capabilities.platformVersion), DeviceDetails.deviceDetailsMap.get(deviceId).get("OSVersion"));
                    capabilities.put(String.valueOf(Capabilities.platformName), DeviceDetails.deviceDetailsMap.get(deviceId).get("platformName"));
                    capabilities.put(String.valueOf(Capabilities.automationName), TestDataController.configDataLists.get("automationName_ios"));
                    capabilities.put(String.valueOf(Capabilities.deviceName), DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName"));
//                    capabilities.put(String.valueOf(Capabilities.appPackage), DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName"));
//                    capabilities.put(String.valueOf(Capabilities.appActivity), DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName"));
//                    capabilities.put(String.valueOf(Capabilities.udid), deviceId);
                } else {
                    log.info("Invalid os name passed while creating node json file..");
                }
                capabilities.put(String.valueOf(Capabilities.maxInstance), Integer.valueOf(TestDataController.configDataLists.get("maxInstance")));
                capabilities.put(String.valueOf(Capabilities.deviceId), deviceId);
                capabilities.put(String.valueOf(Capabilities.systemPort), String.valueOf(8200 + ++count));
                DeviceDetails.deviceDetailsMap.get(deviceId).put("systemPort", String.valueOf(8200 + count));

                array.put(capabilities);
                body.put("capabilities", array);

                JSONObject config = new JSONObject();
                config.put(String.valueOf(Capabilities.cleanUpCycle), Integer.valueOf(TestDataController.configDataLists.get("cleanUpCycle")));
                config.put(String.valueOf(Capabilities.timeOut), Integer.valueOf(TestDataController.configDataLists.get("timeOut")));
                config.put(String.valueOf(Capabilities.proxy), TestDataController.configDataLists.get("proxy"));
                config.put(String.valueOf(Capabilities.maxSession), Integer.valueOf(TestDataController.configDataLists.get("maxSession")));
                config.put(String.valueOf(Capabilities.register), Boolean.valueOf(TestDataController.configDataLists.get("register")));
                config.put(String.valueOf(Capabilities.registerCycle), Integer.valueOf(TestDataController.configDataLists.get("registerCycle")));
                config.put(String.valueOf(Capabilities.hubPort), Integer.valueOf(TestDataController.configDataLists.get("hubPort")));
                config.put(String.valueOf(Capabilities.hubHost), TestDataController.configDataLists.get("hubHost"));
                config.put(String.valueOf(Capabilities.hubProtocol), TestDataController.configDataLists.get("hubProtocol"));

                body.put("configuration", config);

                Writer output = null;
                File file = null;
                if (GlobalVariables.isMAC) {
                    file = new File(GlobalVariables.baseDirPath + TestDataController.configDataLists.get("node_path_in_mac") +
                            "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + ".json");
                } else if (GlobalVariables.isWINDOWS) {
                    file = new File(GlobalVariables.baseDirPath + TestDataController.configDataLists.get("node_path_in_windows") +
                            "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + ".json");
                } else {
                    log.error("Invalid platform..");
                }
                file.getParentFile().mkdirs();
                DeviceDetails.deviceDetailsMap.get(deviceId).put("jsonFilePath", file.getAbsolutePath());
                output = new BufferedWriter(new FileWriter(file));
                output.write(body.toString());
                output.close();
//                ThreadContext.put("logFileName", "framework_log");
                log.info("json file created");
            }
        } catch (Exception e) {
            log.error("Exception has occurred in createDeviceCapabilitiesInJsonFile(): " + e.getMessage());
        }
    }

    public void executeBatchFiles() {
        try {
            for (String deviceId : DeviceDetails.deviceList) {
                if (GlobalVariables.isMAC) {
                    String command = DeviceDetails.deviceDetailsMap.get(deviceId).get("bashFilePath");
                    ServiceUtils.executeShellCommandWithWaitTime(new String[]{command}, true, 10);
                }
                if (GlobalVariables.isWINDOWS) {
                    String command = DeviceDetails.deviceDetailsMap.get(deviceId).get("batchFilePath");
//                    ServiceUtils.executeShellCommandWithWaitTimeQuietly(new String[]{command}, true, 10);
//                    ServiceUtils.executeShellCommandUsingRuntime(new String[]{command});
                   ServiceUtils.executeMe(new String[]{command});
                }
            }
            log.info("batch file executed..");
        } catch (Exception e) {
            log.error("Exception has occurred in executeBatchFiles(): " + e.getMessage());
        }
    }

    public void createExecutableFiles() {
        File file = null;
        try {
            int count = 0;
            for (String deviceId : DeviceDetails.deviceList) {
                FileOutputStream fos = null;
                DataOutputStream dos = null;
                if (GlobalVariables.isMAC) {
                    file = new File(GlobalVariables.baseDirPath + TestDataController.configDataLists.get("node_path_in_mac") +
                            "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + "/node_" +
                            DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + ".command");
                    DeviceDetails.deviceDetailsMap.get(deviceId).put("bashFilePath", file.getAbsolutePath());

                    fos = new FileOutputStream(file);
                    dos = new DataOutputStream(fos);
                    dos.writeBytes(TestDataController.configDataLists.get("bash_command_file_mac"));
                    String newLine = System.getProperty("line.separator");
                    dos.writeBytes(newLine);
                }
                if (GlobalVariables.isWINDOWS) {
                    file = new File(GlobalVariables.baseDirPath + TestDataController.configDataLists.get("node_path_in_windows") +
                            "/node_" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + "/node_" +
                            DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceName") + ".bat");
                    DeviceDetails.deviceDetailsMap.get(deviceId).put("batchFilePath", file.getAbsolutePath());

                    fos = new FileOutputStream(file);
                    dos = new DataOutputStream(fos);
                }
                int portNo = getPort();
                dos.writeBytes("appium -p " + portNo + " --nodeconfig " +
                        DeviceDetails.deviceDetailsMap.get(deviceId).get("jsonFilePath") + " " + "--session-override"
                        + " " + "--local-timezone" + " " + "--log " + GlobalVariables.baseDirPath + "logs/" + portNo + ".log ");
                dos.close();
                DeviceDetails.deviceDetailsMap.get(deviceId).put("deviceNodeId", String.valueOf(portNo));
                log.info("Executable file created");
            }
        } catch (Exception e) {
            log.error("Exception has occurred in createBatchFiles(): " + e.getMessage());
        }
    }

    public void startServerGrid() {
        try {
            killTaskWithPortNumber(TestDataController.configDataLists.get("hubPort"));
            log.info("starting grid server..");
            if (GlobalVariables.isMAC) {
                String command = GlobalVariables.baseDirPath + TestDataController.configDataLists.get("grid_path_in_mac");
                ServiceUtils.executeShellCommand(new String[]{command});
            }
            if (GlobalVariables.isWINDOWS) {
                String command = GlobalVariables.baseDirPath + TestDataController.configDataLists.get("grid_path_in_windows");
                ServiceUtils.executeShellCommand(new String[]{command});
//                ServiceUtils.executeMe(new String[]{command});
            }
            log.info("Grid server started ..");
        } catch (Exception e) {
            log.error("Exception has occurred in startServerGrid(): " + e.getMessage());
        }
    }

    private static int getPort() throws Exception {
        int port = 0;
        try {
            ServerSocket socket = new ServerSocket(0);
            socket.setReuseAddress(true);
            port = socket.getLocalPort();
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return port;

    }

    private void checkNodeRegisteredToGrid() {
        try {
            long timeDiff = 0;
            Date d2 = null;
            Date d1 = new Date();
            Date lastSync_TimeStamp = null;
            boolean inLogVerified = false;
            Date foundInLog = null;
            for (String deviceId : DeviceDetails.deviceList) {
                do {
                    try {
                        d2 = new Date();
                        timeDiff = (((d2.getTime() - d1.getTime()) / 1000) / 60 / 60);//gives the time difference in seconds
                        nodeServiceFilePath = GlobalVariables.baseDirPath + "logs/" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceNodeId") + ".log";
                        nodeRegisteredString = TestDataController.configDataLists.get("nodeRegisteredString");
//                        foundInLog = ServiceUtils.parseLogFileForString(nodeServiceFilePath, nodeRegisteredString, lastSync_TimeStamp);
                        inLogVerified = ServiceUtils.readStringInLog(nodeServiceFilePath, nodeRegisteredString);
                        Assert.assertTrue(inLogVerified);
//                        inLogVerified = true;
                        log.info("Appium Registered String in Log File Verified Successfully for deviceId: " + deviceId);
                        break;
                    } catch (Exception e) {
                        log.debug("Appium Registered String in log file verification pending. Retrying...");
                    } catch (AssertionError afe) {
                        log.debug("Appium Registered String in verification pending. Retrying...");
                    }
                } while (timeDiff < 5 && !inLogVerified);
            }

            if (!inLogVerified)
                throw new Exception("Appium Registered String in Log Verification Failed with max retries..");
        } catch (Exception e) {
            log.error("Exception occurred in checkNodeRegisteredToGrid(): " + e.getMessage());
        }
    }

    public void checkConnectedDeviceStatus() {
        int count = 0;
        boolean inLogVerified = false;
        for (String deviceId : DeviceDetails.deviceList) {
            try {
                do {
                    try {
                        nodeServiceFilePath = GlobalVariables.baseDirPath + "logs/" + DeviceDetails.deviceDetailsMap.get(deviceId).get("deviceNodeId") + ".log";
                        nodeRegisteredString = TestDataController.configDataLists.get("nodeStatusString");
                        inLogVerified = ServiceUtils.readStringInLog(nodeServiceFilePath, nodeRegisteredString);

                    } catch (Exception e) {
                        log.error("Exception in checkConnectedDeviceStatus(): " + e.getMessage());
                    }
                } while ((!inLogVerified) && ((++count) < 3));
                if (!inLogVerified)
                    throw new Exception("Node connection status in grid Verification in log Failed with max retries..");
            } catch (Exception e) {
                log.error("Exception occurred in checkNodeRegisteredToGrid(): " + e.getMessage());
            }
        }
    }

    public static void killTaskWithPortNumber(String portNumber) {
        try {
            if (GlobalVariables.isMAC) {
                String killTaskCommand = TestDataController.configDataLists.get("kill_port_base_command_mac") + ":" + portNumber +
                        TestDataController.configDataLists.get("kill_port_mac_end_point");
                ServiceUtils.executeShellCommand(new String[]{killTaskCommand});
            }
            if (GlobalVariables.isWINDOWS) {
                String killTaskCommand = TestDataController.configDataLists.get("kill_command_portNumber_windows") + " " + portNumber;
                ServiceUtils.executeShellCommand(new String[]{killTaskCommand});
            }
        } catch (Exception e) {
            log.error("Exception has occurred in killTask(): " + e.getMessage());
        }
    }

    public boolean isSeleniumHubRunning(int timeOut) {
        int count = 0;
        while (count < timeOut) {
            try {
                Thread.sleep(1000);
                URL u = new URL("http://localhost:4444/grid/console");
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("GET");  //OR  huc.setRequestMethod ("HEAD");
                huc.connect();
                int code = huc.getResponseCode();
                System.out.println(code);
                return true;
            } catch (Exception e) {
                System.err.println("Selenium Grid is still down.....");
                count++;
                //return false;
            }
        }
        System.err.println("Selenium Grid failed to start up even after   " + timeOut + "  seconds");
        return false;
    }

    public static void main(String[] args) {
        GlobalVariables.init();
        TestDataController.cleanUp();
        new ConfigurationController().createDeviceCapabilitiesInJsonFile();
    }
}
