Feature: Launch App

  Scenario: Launch the app check for ui details
    When App is launched

    Then LoginPage should be displayed
    Then swipeUp to check all languages
    And check continue button is disabled
    Then click on language
    And check Continue button is enabled
    Then Click on Continue button
