
Feature: LogIn_Feature
  In order to access my account
  As a user of the website
  I want to log into the website

  @mytag
  Scenario: App Launch
    When App is launched
    Then LoginPage should be displayed

  Scenario: App Launch with existing user
    When User LogOut from the Application
    Then Successful LogOut message should display